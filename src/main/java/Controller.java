
import dbworker.DBWorker;
import java.io.IOException;
import java.sql.SQLException;
import json.JSONWorker;
import kafka.ConsumerKafka;
import kafka.ProducerKafka;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.pcap4j.core.PcapNativeException;
import sniffer.Sniffer;
import spark.SparkStream;

public class Controller {


    public static void main(String[] args) {
        Sniffer sniffer = new Sniffer();
        JSONWorker jsonWorker = new JSONWorker();
        DBWorker dbWorker = new DBWorker(jsonWorker);
        ProducerKafka kafka = new ProducerKafka(jsonWorker);
        SparkStream sparkStream = new SparkStream(dbWorker, kafka);
//        ConsumerKafka kafkaConsumer = new ConsumerKafka(jsonWorker);

        Logger log = Logger.getLogger(Controller.class);
        PropertyConfigurator.configure("src/main/resources/log4j.properties");

        //поток для работы анализатора трафика
        Thread threadSniff = new Thread(() -> {
            try {
                sniffer.start();
            } catch (PcapNativeException e) {
//                System.out.println("You need to restart app with su if you have Unix system. Example: sudo java -jar testSnif");
//                System.exit(0);
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        threadSniff.setName("snifferThread");
        threadSniff.start();


        Thread threadSpark = new Thread(sparkStream::start);
        threadSpark.setName("sparkThread");
        threadSpark.start();


        // данный поток нужен для получения сбщ об обновлении базы данных
        // но так как в моем проекте ждать ему сбщ не от кого, то и он закомментирован :)
//        Thread threadConKafka = new Thread(() -> {
//            kafkaConsumer.startKafkaConsumer();
//
//            while(!Thread.currentThread().isInterrupted()) {
//                if (kafkaConsumer.hasUpdate()) {
//                    try {
//                        dbWorker.updateLimit();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    } catch (ClassNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//        });
//        threadConKafka.setName("comsumerThread");
//        threadConKafka.start();


        //поток запуска базы данных
        dbWorker.startDB();
        Thread threadDB = new Thread(() -> {
            while(!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1200000);
                    dbWorker.updateLimit();
                } catch (SQLException | ClassNotFoundException | InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        threadDB.setName("dbThread");
        threadDB.start();
    }

}
