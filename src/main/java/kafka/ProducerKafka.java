package kafka;

import java.util.Properties;
import json.JSONWorker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class ProducerKafka {
    private Producer<String, String> producer;
    private final String port;
    private final String ip;

    public ProducerKafka(JSONWorker jsonWorker) {
        this.port = jsonWorker.getPort_kafka();
        this.ip = jsonWorker.getIp_kafka();
    }

    public void startKafkaProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ip + ":" + port);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 60000);
        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "transaction-id");
        producer = new KafkaProducer(props);

    }

    public void send(long size, String s) {
        producer.initTransactions();
        producer.beginTransaction();
        producer.send(new ProducerRecord("alerts", s + size + " bytes. "));
        producer.commitTransaction();
    }

    public Producer<String, String> getProducer() {
        return producer;
    }
}
