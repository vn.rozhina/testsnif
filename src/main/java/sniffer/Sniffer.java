
package sniffer;


import com.sun.jna.Platform;
import org.pcap4j.core.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Sniffer {
    private ArrayList<PcapNetworkInterface> interfaceArrayList;
    private PcapHandle handle;
    private PcapHandle.PcapDirection direction;
    private String ip;
    private final Pattern pattern = Pattern.compile("((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");


    private boolean ipValidate(String ip) {
        return pattern.matcher(ip).matches();
    }

    public void allInterface() {
        try {
            this.interfaceArrayList = (ArrayList) Pcaps.findAllDevs();
            Iterator var1 = this.interfaceArrayList.iterator();

            while(var1.hasNext()) {
                PcapNetworkInterface s = (PcapNetworkInterface)var1.next();
                System.out.println("[" + this.interfaceArrayList.indexOf(s) + "]: " + s.getName());
            }
        } catch (PcapNativeException e) {
            System.out.println("No NIF was found. Please check that install pcap.");
            e.printStackTrace();
        } catch (ClassCastException e) {
            System.out.println("No NIF was found. Please check that install pcap.");
            System.exit(0);
        }

    }

    public void start() throws PcapNativeException, IOException {
        int snapshotLength = 65536;
        int readTimeout = 50;
        ServerSocket serverSocket = new ServerSocket(14141);
        Socket socket = serverSocket.accept();
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        allInterface();
        System.out.println("\nPlease enter number of interface for start sniffing: ");

        Scanner scanner = new Scanner(System.in);
        int num = selectNumInterface(scanner);

        PcapNetworkInterface networkInterface = interfaceArrayList.get(num);
        System.out.println("You chosen interface " + networkInterface.getName());
        handle = networkInterface.openLive(snapshotLength, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, readTimeout);
        System.out.println("\nPlease type ip or press enter. Input format: src/dst <ip>. P.s. Sorry, choice direction is not working on Windows, it`s pcap4j feature.");
        addFilter();
        PacketListener listener = (packet) -> out.println(packet.length());

        try {
            int maxPackets = (int) Double.POSITIVE_INFINITY;
            System.out.println("App started working... You can see progress on log file.");
            this.handle.loop(maxPackets, listener);
        } catch (NotOpenException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void addFilter() {
        Scanner scanner = new Scanner(System.in);
        String filter = "";

        while(true) {
            filter = scanner.nextLine();
            if ("".equals(filter)) {
                System.out.println("You didn`t add filter.");
                break;
            }
            if (this.parseFilter(filter.split(" "))) {
                System.out.println("You add filter.");
                break;
            }
        }

        try {
            if (ip != null) {
                handle.setFilter("ip", BpfProgram.BpfCompileMode.OPTIMIZE, (Inet4Address) InetAddress.getByName(ip));
            }
            if (direction != null) {
                if (Platform.isWindows()) {
                    System.out.println("Sorry, direction choice is not working on Windows. This step will pass.");
                    return;
                }
                handle.setDirection(direction);
            }
        } catch (PcapNativeException e) {
            e.printStackTrace();
        } catch (NotOpenException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }


    private boolean parseFilter(String[] filter) {
        for(String f: filter) {
            if ("src".equals(f)) {
                if (direction == null) {
                    direction = PcapHandle.PcapDirection.IN;
                }
            } else if ("dst".equals(f)) {
                if (direction == null) {
                    direction = PcapHandle.PcapDirection.OUT;
                }
            } else {
                if (!ipValidate(f)) {
                    System.out.println("In filter typo. Please use this template: dst/src <ip_address>. You can enter also ip");
                    return false;
                }
                ip = f;
            }
        }
        return true;
    }

    public int selectNumInterface(Scanner scanner) {
        int num = -1;
        try {
            while(true) {
                num = Integer.valueOf(scanner.next());
                if (num >= 0 && num < this.interfaceArrayList.size()) {
                    return num;
                }
                System.out.println("Please select interface from the above list");
            }
        } catch (NumberFormatException var4) {
            System.out.println("Please enter the interface number");
            return num;
        }
    }
}
